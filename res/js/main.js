$(document).ready(function(){
	App.init();
});

App = {
	init: function(){
		this.initScroll();
	},
	
	initScroll: function(){
		var _self = this;
		$(window).scroll(function(e){
			if($(this).scrollTop() > 75){
				_self.hideHeader();
			}else{
				_self.showHeader();
			}
		});
	},
	
	showHeader: function(){
		$('.header').css('-webkit-transform', 'translate3d(0,0,0)');
	},
	
	hideHeader: function(){
		$('.header').css('-webkit-transform', 'translate3d(0,-25px,0)');
	}
	
};